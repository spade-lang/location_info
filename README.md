# `location_info`

A library to keep track of where in the input source code things come from.

See [https://docs.rs/location_info](https://docs.rs/location_info) for documentation
